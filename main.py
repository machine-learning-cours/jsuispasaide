from fastapi import FastAPI
from pydantic import BaseModel
import joblib
import pandas as pd
from fastapi.middleware.cors import CORSMiddleware

pipeline = joblib.load("data/pipeline.pkl")

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Classe pour les données de prédiction
class CrimePrediction(BaseModel):
    DayOfWeek: str
    Hour: int


@app.post("/predict")
def predict_crime(crime_data: CrimePrediction):
    data_for_prediction = pd.DataFrame({
        "DayOfWeek": [crime_data.DayOfWeek],
        "Hour": [crime_data.Hour]
    })

    predictions = pipeline.predict(data_for_prediction)

    predicted_category = predictions[0, 0]
    predicted_district = predictions[0, 1]

    output_message = f"Le {crime_data.DayOfWeek} à {crime_data.Hour}H, le crime le plus probable est {predicted_category} dans le district {predicted_district}"

    return {"prediction": output_message}
